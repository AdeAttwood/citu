PACKAGE_DRIS:=$(wildcard packages/*)
PACKAGES := $(notdir ${PACKAGE_DRIS})

build: $(foreach package,$(PACKAGES),build-${package})
$(foreach package,$(PACKAGES),build-${package}):
	yarn --cwd packages/$(subst build-,,$@) run build

test: $(foreach package,$(PACKAGES),test-${package})
$(foreach package,$(PACKAGES),test-${package}):
	yarn --cwd packages/$(subst test-,,$@) run test ${ARGS}

install:
	@yarn install
	@./node_modules/.bin/lerna bootstrap

clean:
	@./node_modules/.bin/lerna clean
	rm -rf packages/website/dist

add:
	@./node_modules/.bin/lerna add ${ARGS} --scope ${SCOPE}

start:
	yarn --cwd packages/website run start
