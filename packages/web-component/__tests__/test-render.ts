import * as Citu from '../src';

class MyComponent extends Citu.Component {
    public static get componentName() {
        return 'my-component';
    }

    public render() {
        return `<div>Hello Citu</div>`;
    }
}

Citu.define(MyComponent);

test('The component renders', () => {
    const component = new MyComponent();
    component.update();

    expect(component.innerHTML).toBe('<div>Hello Citu</div>');
});
