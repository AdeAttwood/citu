import * as Citu from '../src';

class MyComponent extends Citu.Component {
    public static get componentName() {
        return 'my-jsx-component';
    }

    public static get tagName() {
        return 'MY-JSX-COMPONENT';
    }

    public render() {
        return <div>Hello JSX Citu</div>;
    }
}

Citu.define(MyComponent);

test('The component renders', () => {
    const component = new MyComponent();
    component.update();

    expect(component.innerHTML).toBe('<div>Hello JSX Citu</div>');
});
