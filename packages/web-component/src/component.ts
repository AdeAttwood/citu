import * as citu from '@citu/core';

export const createElement: citu.CreateElementType = citu.createElement;

export class Component extends HTMLElement {
    private _root: ShadowRoot = null;

    private _rootVNode: citu.VNode = null;
    private _vDom: citu.VNode = null;

    public hasConnected: boolean = false;

    public shadowRootOptions: ShadowRootInit;

    public static get componentName(): string {
        return '';
    }

    public get tagName() {
        const c = this.constructor as any;
        return c.componentName;
    }

    public set tagName(_) {}

    public get root() {
        if (this._root) {
            return this._root;
        }

        this._root = this.attachShadow(
            this.shadowRootOptions || {mode: 'open'},
        );

        this._root.appendChild(document.createElement('div'));

        return this._root;
    }

    public attr(item: string, defaultValue: any = null): any {
        if (this.hasAttribute(item)) {
            return this.getAttribute(item);
        }

        const defaultValues = this.attributeDefaultValues();
        if (item in defaultValues) {
            return defaultValues[item];
        }

        return defaultValue;
    }

    public attributeDefaultValues(): {[key: string]: any} {
        return {};
    }

    public render(): string | HTMLElement | citu.VNode | any {
        return null;
    }

    _render() {
        const newDom = this.render();
        if (newDom === null) {
            return;
        }

        const el = this.shadowRootOptions ? this.root.firstElementChild : this;

        if (!this._rootVNode) {
            this._rootVNode = citu.toVNode(el.cloneNode(true));
        }

        const newVDom = {...this._rootVNode, children: []};

        if (typeof newDom === 'string') {
            const a = document.createElement('div');
            a.innerHTML = newDom;

            Array.from(a.children).forEach(item => {
                newVDom.children.push(citu.toVNode(item));
            });
        } else if (newDom instanceof HTMLElement) {
            newVDom.children.push(citu.toVNode(newDom));
        } else {
            newVDom.children.push(newDom);
        }

        if (!this._vDom) {
            citu.patch(el, newVDom);
        } else {
            citu.patch(this._vDom, newVDom);
        }

        this._vDom = newVDom;
    }

    update() {
        this._render();
    }

    connectedCallback() {
        this.update();
        this.hasConnected = true;
    }

    attributeChangedCallback(attr, a, b) {
        if (this.hasConnected !== true) {
            return;
        }

        this.update();
    }
}

export const define = (component: any) => {
    if (component.componentName !== '') {
        customElements.define(component.componentName, component);
        return;
    }

    console.log(name, component);
};

export default Component;
