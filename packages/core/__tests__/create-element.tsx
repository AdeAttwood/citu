import * as Citu from '../src';

test('Jsx will render', () => {
    const jsx = (
        <div className="wrapper">
            <h1 style={{fontSize: '12px'}}>This is the title</h1>
        </div>
    );

    expect(jsx).toMatchSnapshot();
});

test('Render nested element', () => {
    const MyComponent = () => <div className="child">This is a child</div>;

    const Wrapper = (
        <div className="wrapper">
            <MyComponent prop="one" className={'class-one'} />
        </div>
    );

    const div = document.createElement('div');
    Citu.patch(div, {...Citu.toVNode(div), children: [Wrapper]});

    expect(div.querySelector('.child').innerHTML).toBe('This is a child');
});

test('Render classes as strings', () => {
    const jsx = (
        <div className="wrapper wrapper--one">
            <h1 className={{'class-one': true, 'class-two': false}}>
                This is the title
            </h1>
        </div>
    );

    expect(jsx).toMatchSnapshot();
});
