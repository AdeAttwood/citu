import * as snabbdom from 'snabbdom';
import {toVNode as _toVNode} from 'snabbdom/tovnode';
import {VNode as _VNode} from 'snabbdom/vnode';
import h from 'snabbdom/h';

import ClassModule from 'snabbdom/modules/class';
import AttributeModule from 'snabbdom/modules/attributes';
import StyleModule from 'snabbdom/modules/style';
import EventListenersModule from 'snabbdom/modules/eventlisteners';

export interface VNode extends _VNode {}

export const toVNode = _toVNode;

export const patch = snabbdom.init([
    ClassModule,
    AttributeModule,
    StyleModule,
    EventListenersModule,
]);

export const attributeMap = (attrs, attribtues) => {
    for (const attr in attrs) {
        const value = attrs[attr];

        if (attr.startsWith('on')) {
            const eventName = attr.replace('on', '').toLowerCase();

            attribtues.on[eventName] = value;

            continue;
        }

        if (attr === 'style') {
            attribtues.style = value;
            continue;
        }

        if (attr === 'class' || attr === 'className') {
            attribtues.attrs.class = value;
            continue;
        }

        if (attr === 'classNames') {
            attribtues.attrs.class = Object.keys(value)
                .filter(a => value[a] !== false)
                .join(' ');
            continue;
        }

        attribtues.attrs[attr] = value;
    }

    return attribtues;
};

export type RenderFunction = (props: any) => any;

export type CreateElementType = (
    tag: string | RenderFunction,
    attrs: any,
    ...children: any[]
) => _VNode;

export const createElement: CreateElementType = (tag, attrs, ...children) => {
    const attributes = attributeMap(attrs, {
        attrs: {},
        class: {},
        on: {},
    });

    if (typeof tag === 'function') {
        return tag({...attrs, children: children});
    }

    return h(tag, attributes, children);
};
