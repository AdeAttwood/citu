import * as Citu from '@citu/core';

export const render = (renderFunc: () => any) => {
    const children = renderFunc();
    const container = document.createElement('div');
    Citu.patch(container, { ...Citu.toVNode(container), children: [children] });

    return {
        container,
        getElementsByTagName: container.getElementsByTagName,
        querySelector: container.querySelector,
        querySelectorAll: container.querySelectorAll
    };
};
