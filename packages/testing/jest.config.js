module.exports = {
    preset: 'ts-jest',
    setupFiles: ['@citu/testing/jest.setup.js'],
    testEnvironment: 'node',
    moduleNameMapper: {
        '^.+\\.(css|less|sass|scss)$': __dirname + '/stubs/style.js'
    }
};
