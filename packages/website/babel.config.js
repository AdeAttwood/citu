module.exports = api => {
    const isTest = api.env('test');

    return {
        presets: [
            [
                '@babel/preset-env',
                {
                    targets: {
                        esmodules: true,
                    },
                },
            ],
        ],
        plugins: [
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-syntax-dynamic-import',
            [
                '@babel/plugin-transform-react-jsx',
                {pragma: 'Citu.createElement'},
            ],
        ],
    };
};
