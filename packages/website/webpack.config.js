const c = require('@practically/webpack-4-config');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HandlebarsPlugin = require('handlebars-webpack-plugin');
const SriPlugin = require('webpack-subresource-integrity');

/**
 * Initalize the configuration
 */
c.initialize({
    public_path: '/citu/',
});

/**
 * Add typescript
 */
c.typescript();

/**
 * Define the main index template we want to use
 */
//c.html(path.resolve(__dirname, 'public', 'index.html'));

/**
 * Build the config and name the chunks so that webpack dev server works
 */
const webpackConfig = c.build();
webpackConfig.optimization.splitChunks.name = 'a';

webpackConfig.output.crossOriginLoading = 'anonymous';

webpackConfig.plugins.push(
    new SriPlugin({
        hashFuncNames: ['sha256', 'sha384'],
        enabled: process.env.NODE_ENV === 'production',
    }),
);

webpackConfig.plugins.push(
    new HtmlWebpackPlugin({
        inject: false,
        template: path.join(
            process.cwd(),
            'src',
            'partials',
            'layouts',
            'head.html',
        ),
        filename: path.join(__dirname, 'dist', 'partials', 'head.hbs'),
    }),
);

webpackConfig.plugins.push(
    new HandlebarsPlugin({
        htmlWebpackPlugin: {
            enabled: true,
            prefix: 'html',
        },

        entry: path.join(process.cwd(), 'src', 'pages', '**', '*.hbs'),
        output: path.join(process.cwd(), 'dist', '[path]', '[name].html'),
        data: {},
        partials: [
            'html/partials/*.hbs',
            path.join(process.cwd(), 'src', 'partials', '**', '*.hbs'),
        ],
        helpers: {},
    }),
);

webpackConfig.module.rules[1].oneOf.unshift({
    test: /\.css$/,
    use: [
        {
            loader: 'css-loader',
            options: {
                modules: true,
                sourceMap: process.env.NODE_ENV !== 'production',
            },
        },
        {
            loader: 'postcss-loader',
            options: {
                ident: 'postcss',
                sourceMap: process.env.NODE_ENV !== 'production',
                plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    require('css-mqpacker'),
                    require('postcss-preset-env')({
                        autoprefixer: {
                            flexbox: 'no-2009',
                        },
                        stage: 3,
                    }),
                ],
            },
        },
    ],
});

/**
 * Export the built config
 */
module.exports = webpackConfig;
