import * as Citu from '@citu/web-component';

import './components/slider';

import NavLink from './components/nav-link';
import { CituCode, addLanguage } from './components/code';

addLanguage('tsx');

Citu.define(CituCode);
Citu.define(NavLink);
