import * as Citu from '@citu/web-component';

/**
 * Navigation link component
 */
export class NavLink extends Citu.Component {
    /**
     * The app prop so we can access the router
     */
    public app: any;

    /**
     * Set the app prop when the component is created
     */
    public constructor() {
        super();

        this.app = document.getElementById('app') as any;
    }

    /**
     * The citu component name
     */
    public static get componentName() {
        return 'nav-link';
    }

    /**
     * Test to see if this nav link is the active link
     */
    public get isActive(): boolean {
        const href = this.attr('href');
        return window.location.pathname === href;
    }

    // /**
    //  * Handler for when the component is clicked
    //  */
    // public onClick = (e: any) => {
    //     e.preventDefault();

    //     const href = this.attr('href');
    //     this.app.router.navigate(href);
    //     this.update();
    // };

    /**
     * Renders the component html
     */
    public render() {
        return (
            <li
                classNames={{
                    'p-navigation__link': true,
                    'is-selected': this.isActive
                }}
                role="menuitem"
            >
                <a class="citu-link" href={this.attr('href')}>
                    {this.attr('text')}
                </a>
            </li>
        );
    }
}

export default NavLink;
