import * as Citu from '@citu/web-component';

export class CituSlider extends Citu.Component {
    public static get componentName(): string {
        return 'citu-slider';
    }

    public static get observedAttributes(): Array<string> {
        return ['value'];
    }

    public attributeDefaultValues(): {[key: string]: any} {
        return {value: 50};
    }

    public render() {
        return (
            <div className="p-slider__wrapper">
                <input
                    className="p-slider"
                    type="range"
                    min="0"
                    max="100"
                    value={this.attr('value')}
                    step="1"
                    onInput={e => this.setAttribute('value', e.target.value)}
                />
                <input
                    className="p-slider__input"
                    type="text"
                    maxlength="3"
                    value={this.attr('value')}
                />
            </div>
        );
    }
}

Citu.define(CituSlider);

export class NativeSlider extends HTMLElement {
    public get slider() {
        return this.querySelector('.p-slider');
    }

    public onInput = (e: any) => {
        this.setAttribute('value', e.target.value);
    };

    public static get observedAttributes(): Array<string> {
        return ['value'];
    }

    public attributeChangedCallback(
        attr: string,
        oldValue: any,
        newValue: any,
    ) {
        const input = this.querySelector('.p-slider__input') as any;
        input.value = newValue;
    }

    public connectedCallback() {
        const value = this.hasAttribute('value')
            ? this.getAttribute('value')
            : 50;

        const template = `
            <div class="p-slider__wrapper">
                <input
                    class="p-slider"
                    type="range"
                    min="0"
                    max="100"
                    value="${value}"
                    step="1"
                />
                <input
                    class="p-slider__input"
                    type="text"
                    maxlength="3"
                    value="${value}"
                />
            </div>`;

        this.innerHTML = template;

        this.slider.addEventListener('input', this.onInput);
    }

    public disconnectedCallback() {
        this.slider.removeEventListener('input', this.onInput);
    }
}

customElements.define('native-slider', NativeSlider);
