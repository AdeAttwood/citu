import * as Citu from '@citu/web-component';

const Prism = require('prismjs');
const components = require('prismjs/components');

//import css from 'prismjs/themes/prism-solarizedlight.css';
const css = require('prismjs/themes/prism-solarizedlight.css');
//
console.log(css);

import * as dedent from 'dedent';

export const addLanguage = (language: string): void => {
    if (Prism.languages[language]) {
        return;
    }

    const lang = components.languages[language];
    if (!lang) {
        console.warn(`<citu-code> Unable to add language ${language}.`);
        return;
    }

    if (lang.require && typeof lang.require === 'string') {
        addLanguage(lang.require);
    } else if (lang.require) {
        for (const item of lang.require) {
            addLanguage(item);
        }
    }

    require('prismjs/components/prism-' + language);
};

export class CituCode extends Citu.Component {
    public static get componentName() {
        return 'citu-code';
    }

    public shadowRootOptions = {mode: 'open'} as ShadowRootInit;

    public connectedCallback() {
        this.update();

        const language = this.attr('lang');
        const grammar = Prism.languages[language];

        if (typeof grammar === 'undefined') {
            const languages = Object.keys(Prism.languages).join(', ');
            console.warn(
                `<citu-code> Invalid language ${language}.\n\nThe avaliable languages are ${languages}`,
            );
        }

        const slot = this.root.querySelector('slot');
        const el = this.root.querySelector('code');

        el.innerHTML = Prism.highlight(
            dedent(slot.assignedNodes()[0].textContent),
            grammar || {},
            language,
        );

        this.hasConnected = true;
    }

    public render = () => {
        return (
            <pre>
                <slot hidden></slot>
                <style>{css['0'][3].sourcesContent[0]}</style>
                <code></code>
            </pre>
        );
    };
}

export default CituCode;
