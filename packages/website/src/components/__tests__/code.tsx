import * as Citu from '@citu/web-component';
import {render} from '@citu/testing';

import Code from '../code';

Citu.define(Code);

describe('Citu Code', () => {
    test('The component renders', () => {
        const {container} = render(() => (
            <citu-code lang="ts">
                import * as Citu from '@citu/web-component';
            </citu-code>
        ));

        expect(container.children).toMatchSnapshot();
    });
});
