import * as Citu from '@citu/web-component';

import NavLink from '../nav-link';

Citu.define(NavLink);

describe('Nav Link', () => {
    test('The component renders', () => {
        const navLink = new NavLink();

        navLink.setAttribute('text', 'This is the link text');
        navLink.setAttribute('href', '/where-to-go');
        navLink.update();

        const link = navLink.querySelector('a');

        expect(link).not.toBeNull();
        expect(link.innerText).toBe('This is the link text');

        expect(link.hasAttribute('href')).toBe(true);
        expect(link.getAttribute('href')).toBe('/where-to-go');
    });
});
